.
├── README.md
├── The ExtraSensory Dataset.html
├── The ExtraSensory Dataset_files
│   ├── ExtraSensoryIcon.png
│   ├── example_audio.png
│   ├── example_locations.png
│   ├── example_phone_acc.png
│   ├── example_watch_acc.png
│   ├── extrasensory.css
│   ├── iphoneActive.png
│   ├── iphoneHistory.png
│   ├── iphoneNotification.png
│   └── mobileApp_labelMenu.png
├── dataset
│   ├── ExtraSensory.per_uuid_absolute_location.zip
│   ├── ExtraSensory.per_uuid_features_labels.zip
│   ├── ExtraSensory.per_uuid_mood_labels.zip
│   ├── ExtraSensory.per_uuid_original_labels.zip
│   ├── ExtraSensory.raw_measurements.raw_magnet.zip
│   ├── ExtraSensory.raw_measurements.watch_acc.zip
│   ├── ExtraSensory.raw_measurements.watch_compass.zip
│   ├── README.txt
│   ├── cv5Folds.zip
│   └── intro2extrasensory.ipynb
├── documentation
│   └── robots.txt
├── example
├── robots.txt
├── security.txt
├── structure.txt
├── vaizman2017a_pervasiveAcceptedVersion.pdf
└── vaizman2017b_imwutAcceptedVersion.pdf

5 directories, 28 files
